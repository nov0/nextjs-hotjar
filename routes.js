const routes = require('next-routes')
                                                    // Name   Page      Pattern
module.exports = routes()                           // ----   ----      -----
.add('about')                                       // about  about     /about  
.add({name: 'wizard-new', pattern: '/wizard-new/:page/:userId', page: "wizard-new"})      // (none) complex   /:noname/:lang(en|es)/:wow+