const next = require('next')
const express = require('express')
const server = express();

const routes = require('./routes')

const app = next({dev: process.env.NODE_ENV !== 'production'})
const handler = routes.getRequestHandler(app)


server.get("/api/about", (req, res) => {
    res.send("Hello world");
});

// With express
app.prepare().then(() => {
    server.use(handler).listen(4000)
})
