import React from 'react'
import Link from 'next/link';

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            message: "",
            newbook: {
                key: Date.now(),
                Title: "",
                Author: "",
                Genre: "",
                YearReleased: ""
            }
        };
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({ message: 'Sending...' }, this.sendFormData);
    
    }

    sendFormData() {
        var formData = {
            Title: this.refs.Title.value,
            Author: this.refs.Author.value,
            Genre: this.refs.Genre.value,
            YearReleased: this.refs.YearReleased.value
        };

        if (!formData.Title) {
            setTimeout(() => {
                console.error("Error: title is empty");
                this.setState({ message: 'Error!!!' });
                if(window && window.hj) {
                    console.error("sending hotjar error");
                    window.hj("error", "sdfsdfsd", {
                        error: "error title",
                        testError: "some test error message"
                    })
                }
            })
        } else {
            setTimeout(() => {
                console.log(formData);
                this.setState({ message: 'data sent!' });
            }, 1000);
        }
    }

    render() {
        return (
            <div>
                <h1>New Book</h1>
                {this.state.message}
                <form onSubmit={this.handleSubmit} name="newBookForm">
                    <p> <label for="title">Title</label><input id="title" type="text" ref="Title" name="title"/></p>
                    <p><label for="author">Author</label><input id="author" type="text" ref="Author" name="author"/></p>
                    <p><label for="genre">Genre</label><input id="genre" type="text" ref="Genre" name="genre"/></p>
                    <p><label for="first_published">First Published</label><input id="first_published" type="text" ref="YearReleased" name="firstpublished"/></p>
                    <p><input type="submit" name="subbmit"/></p>
                </form>

                <div>
                    <Link href="/about">
                        <a>Go to about</a>
                    </Link>
                    <br />
                    <Link href="/">
                        <a>Go to home</a>
                    </Link>
                </div>
            </div>
        )
    }
}

export default Form