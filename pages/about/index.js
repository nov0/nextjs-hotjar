
import Link from 'next/link';

const About = () => {
    return (
        <div>
            <div>About</div>
            <Link href="/form">
                <a>Go to form</a>
            </Link>
            <br />
            <Link href="/">
                <a>Go to home</a>
            </Link>
        </div>)
}

export default About