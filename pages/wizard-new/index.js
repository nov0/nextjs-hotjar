
import React, { Component} from 'react';
import Contact from './components/Contact';
import Details from './components/Details';
import Profiles from './components/Profiles';
import { withRouter } from "next/router";
import WizardNavigationPid from './components/WizardNavigationPid';

const initialUser = {
    firstName: "",
    lastName: "",
    nickname: "",
    address: {
        city: "",
        street: "",
        houseNumber: ""
    },
    account: {
        email: "",
        username: "",
        active: true,
        hobby: []
    }
}

class Wizard extends Component {
    state = {
        userData: initialUser
    }

    changeUserData = userData => {
        this.setState(state => ({
            ...state,
            isDataLoading: true,
            userData: {
                ...state.userData,
                ...userData
            }
        }))
    }

    changeDataLoading = isDataLoading => {
        this.setState({
            ...this.state,
            isDataLoading: isDataLoading
        })
    }

    componentDidMount() {
        setTimeout(() => {
            this.changeUserData({
                id: "fb499bb7-b408-4e77-87a0-3f25c9cf6585",
                firstName: "John",
                lastName: "Doe",
                nickname: "",
                address: {
                    city: "Some City",
                    street: "",
                    houseNumber: ""
                },
                account: {
                    email: "",
                    username: "",
                    active: true,
                    hobby: ["running", "hiking", "fpv"]
                }
                
            })
            this.changeDataLoading(false)
        }, 300)
    
    }

    render() {
        let { userData } = this.state;
        let { router: { query: { page } } } = this.props;

        let page = page ? page : Pages.CONTACT;
        return (
            <>
                <div className="page-container">
                    <WizardNavigationPid userData={this.state.userData} />
                    
                    <div className="content">
                        {page === Pages.CONTACT && <Contact userData={this.state.userData} changeUserData={this.changeUserData} />}
                        {page === Pages.DETAILS && <Details userData={userData} changeUserData={this.changeUserData} />}
                        {page === Pages.PROFILE && <Profiles userData={userData} changeUserData={this.changeUserData} />}
                    </div>

                    <style jsx global>{`

                        .page-container {
                            display: flex;
                            flex-direction: row;
                            flex-wrap: nowrap;
                        }

                        nav {
                            background-color: #dbdbdb;
                            width: 120px;
                        }

                        nav a {
                                display: block;
                        }
                        
                        .navigation-button {
                            cursor: pointer;
                        }

                        .navigation-button.active,
                        nav a.active {
                            background-color: yellow;
                        }

                        .content {
                            width: 100%;
                            background-color: #f7fff5;
                        } 

                        `}</style>
                </div>
                <pre>
                    {JSON.stringify(this.state, null, 2)}
                </pre>
            </>
        )
    }
}

export default withRouter(Wizard)

export const Pages = {
    CONTACT: 'contact',
    DETAILS: 'details',
    PROFILE: 'profile'
}
