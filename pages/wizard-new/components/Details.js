import React, { Component} from 'react';
import { Formik } from 'formik';

class Details extends Component {
    handleChange(event) {
        this.props.changeUserData({
            address : { ...this.props.userData.address,[event.target.name]: event.target.value }
        })
    }

    render() {
        return (
            <div>
            <h1>User DETAILS form</h1>
            <Formik
            initialValues={this.props.userData.address}
            onSubmit={(values, actions) => {
                setTimeout(() => {
                    alert(JSON.stringify(values, null, 2));
                    actions.setSubmitting(false);
                }, 1000);
            }}
            >
            {props => {
                return (
                <form onSubmit={props.handleSubmit}>
                    
                    <div>
                        City:
                        <input
                            type="text"
                            onChange={event => {
                                props.handleChange(event)
                                this.handleChange(event);
                            }}
                            value={this.props.userData.address.city}
                            name="city"
                        />
                    </div>
                    
                    <div>
                        Street:
                        <input
                            type="text"
                            onChange={event => {
                                props.handleChange(event)
                                this.handleChange(event);
                            }}
                            value={this.props.userData.address.street}
                            name="street"
                        />
                    </div>
            
                    <div>
                        House Number:
                        <input
                            type="text"
                            onChange={event => {
                                props.handleChange(event)
                                this.handleChange(event);
                            }}
                            value={this.props.userData.address.houseNumber}
                            name="houseNumber"
                        />
                    </div>
                    
                    <br />
                    <button type="submit">Submit</button>
                </form>
            )}}
            </Formik>
        </div>)
        
    }
}

export default Details
