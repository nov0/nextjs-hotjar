import React, { Component} from 'react';
import { Formik } from 'formik';

class Contact extends Component {
    render() {
        return (
        <div>
            <h1>User CONTACTS</h1>
            <Formik
            initialValues={this.props.userData}
            onSubmit={(values, actions) => {
                setTimeout(() => {
                    alert(JSON.stringify(values, null, 2));
                    actions.setSubmitting(false);
                }, 1000);
            }}
            >
            {props => {
                return (
                <form onSubmit={props.handleSubmit}>
                    
                    <div>
                        First Name:
                        <input
                            type="text"
                            onChange={event => {
                                props.handleChange(event)
                                this.props.changeUserData({[event.target.name]: event.target.value})
                            }}
                            value={this.props.userData.firstName}
                            name="firstName"
                        />
                    </div>
                    
                    <div>
                        Last name:
                        <input
                            type="text"
                            onChange={event => {
                                props.handleChange(event)
                                this.props.changeUserData({[event.target.name]: event.target.value})
                            }}
                            value={this.props.userData.lastName}
                            name="lastName"
                        />
                    </div>
            
                    <div>
                        Nickname:
                        <input
                            type="text"
                            onChange={event => {
                                props.handleChange(event)
                                this.props.changeUserData({[event.target.name]: event.target.value})
                            }}
                            value={this.props.userData.nickname}
                            name="nickname"
                        />
                    </div>
                    
                    <br />
                    {/* <button type="submit">Submit</button> */}
                </form>
            )}}
            </Formik>
        </div>)
    }
}

export default Contact
