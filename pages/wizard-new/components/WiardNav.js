import React, { Component } from 'react';
import Link from 'next/link';
import { withRouter } from "next/router";
import routes from "../../../routes";
import { Pages } from '..';

const WizardNav = ({ page, router, isStepValid }) => {
    const { query } = router;
    const { href, as} = routes.findAndGetUrls("wizard-new", { page, userId: query && query.userId ? query.userId : "noid"}).urls;
    const isActive = router.asPath === as;
    return (
        <Link href={href} as={as}>
            <a className={isActive ? "active": ""}>Go to {page} {isStepValid ? <span style={{backgroundColor: "green"}}>√</span> : <span style={{ backgroundColor: "red"}}>X</span>}</a>
        </Link>
    )
}
export default withRouter(WizardNav)