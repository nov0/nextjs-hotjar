import React, { Component} from 'react';
import { Formik } from 'formik';

class Profiles extends Component {

    handleChange(event) {
        this.props.changeUserData({
            account : { ...this.props.userData.account, [event.target.name]: event.target.value }
        })
    }

    render() {
        return (
            <div>
            <h1>User PROFILE form</h1>
            <Formik
            initialValues={this.props.userData.account}
            onSubmit={(values, actions) => {
                setTimeout(() => {
                    alert(JSON.stringify(this.props.userData, null, 2));
                    actions.setSubmitting(false);
                }, 1000);
            }}
            >
            {props => {
                return (
                <form onSubmit={props.handleSubmit}>
                    
                    <div>
                        Email:
                        <input
                            type="text"
                            onChange={event => {
                                props.handleChange(event)
                                this.handleChange(event);
                            }}
                            value={this.props.userData.account.email}
                            name="email"
                        />
                    </div>
                    
                    <div>
                        Username:
                        <input
                            type="text"
                            onChange={event => {
                                props.handleChange(event)
                                this.handleChange(event);
                            }}
                            value={this.props.userData.account.username}
                            name="username"
                        />
                    </div>

                    <br />
                    <button type="submit">Submit</button>
                </form>
            )}}
            </Formik>
        </div>)
        
    }
}

export default Profiles
