
import React, { Component, useReducer } from 'react';
import Link from 'next/link';
import WizardNav from "./WiardNav";
import { Pages } from '..';

const WizardNavigation = ({ userData }) => {

    const isContactValid = () => {
        return userData && userData.firstName && userData.lastName && userData.nickname;
    }

    const isDetailsValid = () => {
        return userData && userData.address && userData.address.city && userData.address.street && userData.address.houseNumber;
    }

    const isAccountValid = () => {
        return userData && userData.account && userData.account.email && userData.account.username;
    }

    return (
        <nav>
            <div>:::WIZARD NAVIGATION:::</div>
            <hr />
            <WizardNav
                isStepValid={isContactValid()}
                page={Pages.CONTACT}
            />
            <WizardNav
                isStepValid={isDetailsValid()}
                page={Pages.DETAILS}
            />
            <WizardNav
                isStepValid={isAccountValid()}
                page={Pages.PROFILE}
            />
            <hr />
            <Link href="/">
                <a>Go to home</a>
            </Link>
        </nav>
    )
}

export default WizardNavigation
