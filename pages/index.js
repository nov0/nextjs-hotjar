import Head from 'next/head'
import Link from 'next/link';

const Home = () => (
  <div className="container">
    <Head>
      <title>Create Next App</title>
      <link rel="icon" href="/favicon.ico" />
      <script
        dangerouslySetInnerHTML={{
          __html: `
              (function(h,o,t,j,a,r){
                h.hj = h.hj || function () { (h.hj.q = h.hj.q || []).push(arguments) }
              h._hjSettings={hjid:1713540,hjsv:6};
              a=o.getElementsByTagName('head')[0];
              r=o.createElement('script');r.async=1;
              r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
              a.appendChild(r);
          })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');`
        }}
      />
      <script
        dangerouslySetInnerHTML={{
          __html: `
          var userId = "123456" || null; // Replace your_user_id with your own if available.
          window.hj('identify', userId, {
              'Signed up': '2019—06-20Z', // Signup date in ISO-8601 format.
              'test': 'test variable'
              // Add your own custom attributes here. Some examples:
              // 'Last purchase category': 'Electronics', // Send strings with quotes around them.
              // 'Total purchases': 15, // Send numbers without quotes.
              // 'Last purchase date': '2019-06-20Z', // Send dates in ISO-8601 format. 
              // 'Last refund date': null, // Send null when no value exists for a user.
          })`
        }}
      />
    </Head>

    <main>
      <h1 className="title">
        Welcome to Next.js demo
      </h1>

      <p className="description">
        Get started by editing <code>pages/index.js</code>
      </p>
      <div className="grid">
        <Link href="form">
          <a
            className="card"
          >
            <h3>Form &rarr;</h3>
            <p>
              Got to Form internal page
          </p>
          </a>
        </Link>

        <Link href="about">
          <a
            href="https://zeit.co/new?filter=next.js&utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
            className="card"
          >
            <h3>About &rarr;</h3>
            <p>
              Go to about internal page
          </p>
          </a>
        </Link>

        <Link href="fullscreen">
          <a
            className="card"
          >
            <h3>Full Screen &rarr;</h3>
            <p>
              Go to next page to fullscreen
          </p>
          </a>
        </Link>
        {/* <Link href="lockorientation">
          <a
            className="card"
          >
            <h3>Lock Orientation &rarr;</h3>
            <p>
              Go to next page to loca orientaiton on mobile devices
          </p>
          </a>
        </Link> */}

        <Link href="/user/contact">
          <a
            className="card"
          >
            <h3>Navigation with router Orientation &rarr;</h3>
            <p>
              Go to pages that are navigation trough router switch.
          </p>
          </a>
        </Link>

        <Link href="/wizard">
          <a
            className="card"
          >
            <h3>Wizard &rarr;</h3>
            <p>
              Go to Wizard.
          </p>
          </a>
        </Link>

        <Link href="/wizard-new/contact/fb499bb7-b408-4e77-87a0-3f25c9cf6585">
          <a
            className="card"
          >
            <h3>Wizard New&rarr;</h3>
            <p>
              Go to Wizard New.
          </p>
          </a>
        </Link>



        {/* 
        <a className="card">
          <h3>Documentation &rarr;</h3>
          <p>Find in-depth information about Next.js features and API.</p>
        </a>

        <a className="card">
          <h3>Learn &rarr;</h3>
          <p>Learn about Next.js in an interactive course with quizzes!</p>
        </a> */}


        <a
          href="https://surveys.hotjar.com/s?siteId=1713540&surveyId=152766"
          className="card"
          arget="_blank"
        >
          <h3>Survey &rarr;</h3>
          <p>Please answer survey question created by HOTJAR</p>
        </a>

        <a
          href="https://zeit.co/new?filter=next.js&utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          className="card"
        >
          <h3>Deploy &rarr;</h3>
          <p>
            Instantly deploy your Next.js site to a public URL with ZEIT Now.
          </p>
        </a>
      </div>
    </main>

    <footer>
      <a
        href="https://zeit.co?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
        target="_blank"
        rel="noopener noreferrer"
      >
        Powered by <img src="/zeit.svg" alt="ZEIT Logo" />
      </a>
    </footer>

    <style jsx>{`
      .container {
        min-height: 100vh;
        padding: 0 0.5rem;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }

      main {
        padding: 5rem 0;
        flex: 1;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }

      footer {
        width: 100%;
        height: 100px;
        border-top: 1px solid #eaeaea;
        display: flex;
        justify-content: center;
        align-items: center;
      }

      footer img {
        margin-left: 0.5rem;
      }

      footer a {
        display: flex;
        justify-content: center;
        align-items: center;
      }

      a {
        color: inherit;
        text-decoration: none;
      }

      .title a {
        color: #0070f3;
        text-decoration: none;
      }

      .title a:hover,
      .title a:focus,
      .title a:active {
        text-decoration: underline;
      }

      .title {
        margin: 0;
        line-height: 1.15;
        font-size: 4rem;
      }

      .title,
      .description {
        text-align: center;
      }

      .description {
        line-height: 1.5;
        font-size: 1.5rem;
      }

      code {
        background: #fafafa;
        border-radius: 5px;
        padding: 0.75rem;
        font-size: 1.1rem;
        font-family: Menlo, Monaco, Lucida Console, Liberation Mono,
          DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace;
      }

      .grid {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-wrap: wrap;

        max-width: 800px;
        margin-top: 3rem;
      }

      .card {
        margin: 1rem;
        flex-basis: 45%;
        padding: 1.5rem;
        text-align: left;
        color: inherit;
        text-decoration: none;
        border: 1px solid #eaeaea;
        border-radius: 10px;
        transition: color 0.15s ease, border-color 0.15s ease;
      }

      .card:hover,
      .card:focus,
      .card:active {
        color: #0070f3;
        border-color: #0070f3;
      }

      .card h3 {
        margin: 0 0 1rem 0;
        font-size: 1.5rem;
      }

      .card p {
        margin: 0;
        font-size: 1.25rem;
        line-height: 1.5;
      }

      @media (max-width: 600px) {
        .grid {
          width: 100%;
          flex-direction: column;
        }
      }
    `}</style>

    <style jsx global>{`
      html,
      body {
        padding: 0;
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
          Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
      }

      * {
        box-sizing: border-box;
      }

      .page-container {
        display: fles;
      }
    `}</style>
  </div>
)

export default Home
