
import React, { Component } from 'react';
import Link from 'next/link';
import { withRouter } from "next/router";

class WizardNavigation extends Component {

    render() {
        const { setTabName, tabName } = this.props;
        return (
            <nav>
                <div>:::WIZARD NAVIGATION:::</div>

                <div className={`navigation-button ${tabName === "Contact" ? "active" : ""}`}
                    onClick={() => setTabName("Contact")}>
                    Contact
                </div>

                <div className={`navigation-button ${tabName === "Details" ? "active" : ""}`}
                    onClick={() => setTabName("Details")}>
                    Details
                </div>

                <div className={`navigation-button ${tabName === "Profile" ? "active" : ""}`}
                    onClick={() => setTabName("Profile")}>
                    Profile
                </div>

                <Link href="/">
                    <a>Go to home</a>
                </Link>
            </nav>
        )
    }
}


export default withRouter(WizardNavigation)
