
import React, { Component} from 'react';
import WizardNavigation from './components/WizardNavigation';
import Contact from './components/Contact';
import Profiles from './components/Profiles';
import Details from './components/Details';
import { withRouter } from "next/router";

class Wizard extends Component {

    static async getInitialProps(ctx){

        const { page, userId } = ctx.query;
        console.log("PAGE: ", page);
        console.log("USER_ID: ", userId)
        console.log(ctx)

        return {
          page,
          userId
        };
      }


    state = {
        tabName: "Contact",
        userData: {}
    }

    setTabName = tabName => {
        this.setState( {
            ...this.state,
            tabName: tabName
        })
    }

    changeUserData = userData => {
        this.setState({
            ...this.state,
            userData: {
                ...this.state.userData,
                ...userData
            }
        })
    }

    render() {
        const { tabName, userData } = this.state;
        return (
            <div className="page-container">

                <WizardNavigation
                    setTabName={this.setTabName}
                    tabName={tabName}
                />
                
                <div className="content">
                    {tabName === "Contact" && <Contact userData={userData} changeUserData={this.changeUserData} />}
                    {tabName === "Details" && <Details userData={userData} changeUserData={this.changeUserData} />}
                    {tabName === "Profile" && <Profiles userData={userData} changeUserData={this.changeUserData} />}
                </div>
                

                <style jsx global>{`

                    .page-container {
                        display: flex;
                        flex-direction: row;
                        flex-wrap: nowrap;
                    }

                    nav {
                        background-color: #dbdbdb;
                        width: 120px;
                    }

                    nav a {
                            display: block;
                    }
                    
                    .navigation-button {
                        cursor: pointer;
                    }

                    .navigation-button.active {
                        background-color: yellow;
                    }

                    .content {
                        width: 100%;
                        background-color: #f7fff5;
                    } 

                    `}</style>
            </div>
        )
    }
}

export default withRouter(Wizard)