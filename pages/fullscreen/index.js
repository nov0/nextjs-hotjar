
import React, { Component} from 'react';
import Link from 'next/link';

class FullScreen extends Component {

    enterFullScreen = () => {
        if (isClientSideRendering() &&!document.fullscreenElement) {
            document.documentElement.requestFullscreen();
        }
    }

    exitFullScreen = async () => {
        if (isClientSideRendering() && document.exitFullscreen) {
            await document.exitFullscreen().catch(error => {
                console.log(document.exitFullscreen)
                console.error(error)
            })
        }
    }

    componentDidMount() {
        this.enterFullScreen()
    }

    componentWillUnmount() {
        this.exitFullScreen();
    }

    render() {
        return (
            <div>
                <div>You should be now in fullscreen</div>
                <Link href="/">
                    <a>Go to home</a>
                </Link>
            </div>)
    }
}

const isClientSideRendering = () => {
    return typeof window !== 'undefined'
}


export default FullScreen