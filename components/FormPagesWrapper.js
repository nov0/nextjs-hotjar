import React, { Component } from 'react';
import Navigation from './Navigation';


///// FAIIIL CANT UPDATE FORM FROM CHILDREN
class FormPagesWrapper extends Component {

    state = {
        constactData: {
            name: "novo"
        }
    }

    updateContactData = data => {
        console.log(data);
        this.setState(state, {
            constactData: {
                ...state.constactData,
                ...data
            }
        })
    }

    render() {
        const { constactData } = this.state;
        return (
            <div className="page-container ">

                <Navigation constactData={constactData}/>

                {/* <div className="content">{children}</div> */}
                <div className="content">{React.cloneElement(this.props.children, {...this.props, updateContactData: this.updateContactData})}</div>














                <style jsx global>{`

                    .page-container {
                        display: flex;
                        flex-direction: row;
                        flex-wrap: nowrap;
                    }

                    nav {
                        background-color: #dbdbdb;
                        width: 200px;
                    }

                    nav a {
                            display: block;
                    }

                    .content {
                        width: 100%;
                        background-color: #f7fff5;
                    } 

                    `}</style>
            </div>)
    }
}

export default FormPagesWrapper