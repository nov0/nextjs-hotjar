import React, { Component} from 'react';
import Link from 'next/link';

class Navigation extends Component {

    render() {
        return (
            <nav>
                <div>:::NAVIGATION:::</div>
                <Link href="/user/contact">
                    <a>CONTACT</a>
                </Link>
                <Link href="/user/details">
                    <a>DETAILS</a>
                </Link>
                <Link href="/user/profile">
                    <a>PROFILE</a>
                </Link>
                <Link href="/">
                    <a>Go BACK</a>
                </Link>
            </nav>)
            
    }
}


export default Navigation